#!/usr/bin/perl
#
#	This script replaces occurences of #PROJECT_HOME#, #PROJECT_NAME# and #PROJECT_DOMAIN#
#	with the project relevant data provided through the 'project.properties' file.
#		
#	The script must be called with the properties file as the first and the desired browser (chrome, firefox or safari) as the second argument	
#
#	mve, 17-08-12 initial version
#	mve, 01-04-13 added PUMA server list
# 	mve, 05-06-13 build per properties file

use strict;
use Encode;
use File::Find;
use File::Path;
use File::Copy;

# some configs
my $placeholderFolder = "misc/";
my $file=$File::Find::name;
my $mainBuildPath = "build";
my $extensionBaseDir = "extension";

# to set the min version for firefox we must set the minimum version
# the key applications is not a valid manifest entry for chrome
# so we have to add it with this script
my %browserManifest = (
    chrome    => "",
    firefox   => "  \"applications\": {
    \"gecko\": {
      \"id\": \"buttons@#PROJECT_DOMAIN#\",
      \"strict_min_version\": \"56.0\",
      \"update_url\": \"https://www.bibsonomy.org/puma_files/firefox/update_manifest.json\"
    }
  },\n"
);

# to make the Background page non persistent thus making it an event page
# Firefox currently (as of Version 93) does not support non Event Pages and prompts a warning 
# Safari requires a non persistant Background Page to work on IOS and iPadOS
my %eventPage = (
    chrome    => "",
    firefox   => "",
	safari    => ", \"persistent\": false\n"		
);

# load and check the arguments
die "Usage: $0 PROPERTIES_FILE BROWSER\n" if @ARGV < 2;
my ($propertiesFile, $browser) = @ARGV;

# first create the build dir
my @projectDependantFiles = ();
my @ofRelevantFiletype = ();
my @extSrc = ();

my $project = $1 if ($propertiesFile=~/(^[a-zA-Z0-9]+)/);
my $projectBuildPath = $mainBuildPath . "/" . $project;
my $buildPath = $projectBuildPath . "/" . $browser;

# clean the build dir
if (-e $buildPath) {
	print "cleaning build path $buildPath\n";
	rmtree($buildPath) or die "No dir $mainBuildPath!";
}

# read in properties file 
open(PROPERTIES_FILE, $propertiesFile) or die ("file at '$propertiesFile' not found");

my $projectHome = "";
my $projectDomain = "";
my $projectListUrl = "";
my $showDialog = 0;
my $iconPath = "";
my $projectName = "";
my $projectNameLC = "";
my @icons = ();

foreach my $line (<PROPERTIES_FILE>) {
	if ($line=~/project\.(home|name|listUrl|iconPath)/) {
		if ($1=~/home/) {
			$line=~s/^\s+|(project.home|[ =]*)|\s+$//g;
			$projectHome=$line;
			
			# extract the domain from the url
			$projectDomain=$projectHome;
			$projectDomain=~s/^\s+|(^(https?:\/\/(www.){0,1})|\/[a-z]*)|\s+$//g;
		} elsif($1=~/listUrl/) {
			$line=~s/^\s+|(project.listUrl|[ =]*)|\s+$//g;
			$projectListUrl=$line;
			$showDialog=1;
		} elsif($1=~/iconPath/) {
			$line=~s/^\s+|(project.iconPath|[ =]*)|\s+$//g;
			$iconPath=$line;
		} else {
			$line=~s/^\s+|(project.name|[ =]*)|\s+$//gs;
			$projectName=$line;
			$projectNameLC=lc($line);
		}
	} 
}
close(PROPERTIES_FILE);

my $browserMainifestReplacement = $browserManifest{$browser};
my $eventPageReplacement = $eventPage{$browser};

# create build dir structure
if (not -e $mainBuildPath) {
	print "creating build dir";
	mkdir($mainBuildPath) or die "no dir $mainBuildPath";
}

if (not -e $projectBuildPath) {
	mkdir($projectBuildPath) or die "no dir $projectBuildPath";
}

mkdir($buildPath) or die "no dir $buildPath";

# copy the files
copyFolder("src/webextension/", $buildPath);

find(\&findRelevantFiles, "$buildPath");

if (length $iconPath) {
	replaceIcons(); 
}

# replace placeholders
# in files containing placeholders defined at the top of this document
foreach my $file (@ofRelevantFiletype) {

	my $line = 1;
	if($file=~/[\~]PROJECT_DEPENDANT$/ ) {
		
		if($showDialog) {
			my $newFilename=$file;
			$newFilename=~s/[\~]PROJECT_DEPENDANT//g;
			rename($file, $newFilename);
			$file=$newFilename;
		} else {
			unlink($file);
			next;
		}
	}

	open(FILE, "<", "$file") or die("could not open $file");
	open(COPY, "+>", "$file~") or die("could not create $file~");

	foreach (<FILE>) {
		# the browser manifest replacement can include placeholders, so we first replace the manifest includes
		if (/\#BROWSER_MANIFEST_INCLUDE\#/) {
			$_ = $browserMainifestReplacement
		}
		if (/\#EVENT_PAGE_INCLUDE\#/) {
			$_ = $eventPageReplacement
		}
		if (/\#(PROJECT_)/) {
			$_ = replacePlaceholders($_);
		} elsif (/\#INCLUDE\#/) {
			if ($showDialog) {
				push (@projectDependantFiles, $file);
			} else {
				$_ = "";
			}
		}
		print COPY;
		$line++;
	}
	close(FILE);
	close(COPY);
	unlink($file);
	rename("$file~", $file);
}

# some files are project dependent
# for every '#INCLUDE#' line in the source
# we replace the line with the content of the file
# in the configured project dependent folder
# e.g. for puma we need a server list ui to select
# the puma instance to use
foreach my $file (@projectDependantFiles) {
	my $line = 1;
	my $folder = $file;
	my $filename;
	my $replacementFile;
	my $content = "";
	
	# extract the folder from the file
	$folder =~ s/([a-zA-Z.#_\@]*)$//;
	$filename=$1;
	$folder=~s/ROOT_[A-Za-z0-9\.\@_\#]+\//$placeholderFolder/g;
	
	$replacementFile = $folder.$filename;
	$replacementFile =~ s/\Q$buildPath\/$extensionBaseDir/src\/webextension\/$placeholderFolder/g;
	if (-e $replacementFile) {
		if ($showDialog) {
			$content = getPlaceholderContent($replacementFile);
		}

		open(COPY, "+>$file~") or die("could not create $file~");
		open(FILE, "<$file");	
		foreach (<FILE>) {
			$_ = replacePlaceholders($content) if(/\#INCLUDE\#/); 
			print COPY;
			$line++;
		}
		close(FILE);
		close(COPY);
		unlink($file);
		rename("$file~", $file);
	}
}

my $resultFileName = "$browser.zip";
# ready now zip the file
system("(cd $buildPath/$extensionBaseDir; zip -rq ../$resultFileName *)");

sub copyFolder {
	my ($srcDir, $dstDir) = @_;
	opendir my($swap), $srcDir;
	for my $item (readdir $swap) {
		next if $item =~ /^(CVS|\.[\.]*|$placeholderFolder)$/;
		my $src = "$srcDir/$item";
		my $dst = "$dstDir/$item";
		
		if (-d $src) {
			mkdir $dst;
			copyFolder($src, $dst);
		} else {
			copy($src, $dst);
		}
	}
	closedir $swap;
	return;
}

sub findRelevantFiles {
	my $file=$File::Find::name;
	push(@extSrc, $file) if ($file=~/ROOT_[A-Za-z0-9\.\@_\#]+$/);
	push(@ofRelevantFiletype, $file) if ($file=~/\.(js|dtd|properties|gif|manifest|rdf|json|xul|css|html)(\~PROJECT_DEPENDANT)*$/);
	if ($file=~/\.(png|jpg|gif)$/) {
		push(@icons, $file);
	}
}
	
sub replaceIcons {
	foreach my $icon(@icons) {
		my $replacementIcon = $icon;
		$replacementIcon=~s/^\Q$buildPath\/$extensionBaseDir\//$iconPath/g;
		if (-f $replacementIcon) {
			copy($replacementIcon, $icon);
			print "replace $icon with $replacementIcon\n";
		}
	}
}

sub getPlaceholderContent {
	my ($file) = @_;
	open(FILE, "<$file") or die "file $file not found";
	my $content = "";
	foreach my $line(<FILE>) {
		$content=$content.$line;
	}
	close(FILE);
	return $content;
}

sub replacePlaceholders {
	my ($newName) = @_;
	$newName=~s/\#PROJECT_HOME\#/$projectHome/g;
	$newName=~s/\#PROJECT_NAME\#/$projectName/g;
	$newName=~s/\#PROJECT_DOMAIN\#/$projectDomain/g;
	$newName=~s/\#PROJECT_NAME_LC\#/$projectNameLC/g;
	$newName=~s/\#PROJECT_DIALOG\#/$showDialog/g;
	$newName=~s/\#PROJECT_SERVER_LIST_URL\#/$projectListUrl/g;
	return $newName;
}