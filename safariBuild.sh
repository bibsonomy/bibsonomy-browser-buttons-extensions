#!/bin/bash
THEME=$1
BUILD="${2:-false}"

perl build_extensions.pl $THEME.properties safari

if [ "$THEME" == "puma-wue-test" ]; then
  THEME="puma"
fi

BUILD_PATH=$PWD/build/$THEME/safari
OUTPUT_PATH=$BUILD_PATH/XcodeProject
EXTENSION_PATH=$BUILD_PATH/safari

cd $BUILD_PATH
unzip safari.zip -d $EXTENSION_PATH

mkdir -p $OUTPUT_PATH
cd $OUTPUT_PATH 

xcrun safari-web-extension-converter --copy-resources --force --app-name "$THEME" $EXTENSION_PATH
echo "Safari version was generated."

if [ "$BUILD" == "true" ]; then
	SCHEME="$THEME (macOS)"
	cd $OUTPUT_PATH/$THEME
	xcodebuild -scheme "$SCHEME" build -derivedDataPath "$OUTPUT_PATH/Export"
	echo "macOS Version was exported."
fi
