var PROJECT_URL = "#PROJECT_HOME#";

//opens the given url with the user set preference (new tab or current tab)
function invokeAction(url, tab) {
	chrome.storage.local.get('openNewTab', function (e) {
		var openNewTab = e.openNewTab;
		if (typeof openNewTab === "undefined") {
			chrome.tabs.update(tab.id, { 'url': url });
		} else if (openNewTab == true) {
			chrome.tabs.create({ 'url': url });
		} else {
			chrome.tabs.update(tab.id, { 'url': url });
		}
	});
}

//opens the project home
function openHome(tab) {
	chrome.tabs.query({ active: true, currentWindow: true }, function (tab) {
		tab = tab[0];
		getProjectHome().then(function (home) {
			invokeAction(home + 'myBibSonomy', tab);
		});
	});
}

// registers the shortcut commands defined in manifest.json
chrome.commands.onCommand.addListener(function (command) {
	if (command == "openHome") {
		openHome();
	}
	if (command == "saveBookmark") {
		savePost(true);
	}
	if (command == "savePublication") {
		savePost(false);
	}
});

//Necessary for the pearl script to inject the functions to open the PUMA Instance select page
#INCLUDE#

//returns the project home url as selected by the user or defined in the manifest.json as default
async function getProjectHome() {
	var projectHome = new Promise(function (resolve, reject) {
		chrome.storage.local.get('pumaInstance', function (e) {
			var instanceData = e.pumaInstance;
			if (typeof instanceData !== "undefined") {
				var home = instanceData.instanceUrl;
				resolve(home);
			} else {
				resolve(PROJECT_URL);
			}
		})
	});
	const out = await projectHome;
	return out;
}

//saves the current page as a bookmark or publication
function savePost(saveBookmark) {
	chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		var tab = tabs[0];
		var url = tab.url;

		// about:// and chrome:// are not supported by executeScript api
		if (url.startsWith("about://") || url.startsWith("chrome://")) {
			openEditView(saveBookmark, tab, "");
		} else {
			// get the selected text in the active tab, requires "activeTab" permission
			chrome.tabs.executeScript(tab.id, { code: "window.getSelection().toString();" }, function (selection) {
				openEditView(saveBookmark, tab, selection);
			});
		}
	});
}

function openEditView(saveBookmark, tab, selection) {
	var url = tab.url;
	chrome.storage.local.get("visibility", function (e) {
		var vis = 'public';
		if (typeof e.visibility !== "undefined" && e.visibility == "private") {
			vis = 'private';
		}
		getProjectHome().then(function (home) {
			invokeAction(home + (saveBookmark ? 'ShowBookmarkEntry?' : 'BibtexHandler?requTask=upload&') + 'selection='
				+ encodeURIComponent(selection)
				+ '&url='
				+ encodeURIComponent(url)
				+ '&referer='
				+ encodeURIComponent(url)
				+ '&description='
				+ encodeURIComponent(tab.title)
				+ '&abstractGrouping='
				+ vis
				+ '&autoLogin=true',
				tab);
		});
	});
}

//adds onMessage listeners to the background script to receive messages from the popup and the options page
chrome.runtime.onMessage.addListener((request) => {
	if (request.type == "Open home Request") {
		openHome();
	}

	if (request.type == "Save bookmark Request") {
		savePost(true);
	}

	if (request.type == "Add publication Request") {
		savePost(false);
	}
});