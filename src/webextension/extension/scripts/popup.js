document.addEventListener('DOMContentLoaded',
	function () {
		var buttons = new Array();
		var buttonPrefix = ["home", "bookmark", "publication"];
		var optionsArea = document.getElementById("optionsArea");
		var cogwheel = document.getElementById("cogwheel");
		var optionsButton = document.getElementById("optionsButton");
		var controlArea = document.getElementById("controlArea");
		var locales = document.getElementsByTagName("localestring");
		
		//Necessary for the pearl script to inject the PUMA Instance select options
		#INCLUDE#

		restoreOptions();

		//Sets all text elements to the correct language
		for (var i = 0; locales.length > i; i++) {
			locales[i].innerHTML = chrome.i18n.getMessage(locales[i].innerHTML);
		}
		optionsButton.title = chrome.i18n.getMessage("showOptionsLabel");

		for (var i = 0; i < buttonPrefix.length; i++) {
			buttons.push(document.getElementById(buttonPrefix[i] + "Button"));
			buttons[i].title = chrome.i18n.getMessage(buttonPrefix[i] + "ButtonTitle");
		}
		
		//Shows the Browser buttons and hides the options menue 
		optionsArea.style.display = "none";
		cogwheel.style.display = "none";
		controlArea.style.visibility = "visible";

		//Adds the eventlistener to the options button
		var publicVisibilityButton = document.getElementById("radioButtonPublic");
		publicVisibilityButton.addEventListener('click', function () {
			changeVisibility("public");
		});

		var privateVisibilityButton = document.getElementById("radioButtonPrivate");
		privateVisibilityButton.addEventListener('click', function () {
			changeVisibility("private");
		});

		var openInNewHomeSetting = document.getElementById("checkboxOpenNewTab");
		openInNewHomeSetting.addEventListener('click', function () {
			chrome.storage.local.set({ 'openNewTab': openInNewHomeSetting.checked });
		})

		//Adds the eventlistener to the Browser buttons
		buttons[0].addEventListener('click', function () {
			chrome.runtime.sendMessage({ type: "Open home Request" });
			window.close();
		});
		buttons[1].addEventListener('click', function () {
			chrome.runtime.sendMessage({ type: "Save bookmark Request" });
			window.close();
		});
		buttons[2].addEventListener('click', function () {
			chrome.runtime.sendMessage({ type: "Add publication Request" });
			window.close();
		});

		optionsButton.addEventListener('click',
			//Switches between the Browser buttons and the options menue
			function (e) {
				var visibility = "none";
				var indicator = "cogwheel";
				var optionsButtonClass = "spacer";
				optionsButton.title = chrome.i18n.getMessage("showOptionsLabel");

				if (optionsArea.style.display == "none") {
					visibility = "";
					indicator = "contract";
					optionsButtonClass = "";
					restoreOptions();
					optionsButton.title = chrome.i18n.getMessage("hideOptionsLabel");
				}
				controlArea.style.display = optionsArea.style.display;
				optionsArea.style.display = visibility;
				cogwheel.style.display = visibility;
				e.target.src = "../images/" + indicator + ".png";
				e.target.className = optionsButtonClass;
			}
		);
						
	}
);

//Restores the checkbox state using the preferences or default values
function restoreOptions() {
	chrome.storage.local.get(null, function setCurrentChoice(result) {
		document.querySelector("#checkboxOpenNewTab").checked = result.openNewTab || false;
		document.querySelector("#radioButtonPublic").checked = result.visibility == "public" || true;
		document.querySelector("#radioButtonPrivate").checked = result.visibility == "private" || false;
	});
}

function changeVisibility(visibility) {
	chrome.storage.local.set({ 'visibility': visibility });
}