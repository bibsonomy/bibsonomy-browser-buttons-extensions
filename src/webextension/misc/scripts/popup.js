//Forces the user to select a PUMA Instance before using the Extension for the first time
chrome.storage.local.get('hasRun', function (e) {
	var hasRun = e.hasRun;
	if (typeof hasRun === "undefined") {
		chrome.runtime.sendMessage({ type: "Show server list" });
		window.close();
	}
});

chrome.runtime.sendMessage({ type: "Check last update"});
var serverUrl = "#PROJECT_SERVER_LIST_URL#";
var changePumaServerDropdown = document.getElementById("changePumaServerDropdown");

const serverMap = new Map();
//downloads the list of puma servers from the server
if (XMLHttpRequest) {
	var xhr = new XMLHttpRequest();
	if ("withCredentials" in xhr) {
		xhr.onload = function () {
			var data = JSON.parse(this.responseText);
			//fills the server list dropdown with every server in the recieved data
			for (var i in data.server) {
				var instanceData = data.server[i];
				if(!instanceData.active)
					continue;
				var opt = document.createElement("option");
				opt.value= instanceData.instanceUrl;
				opt.innerHTML = instanceData.instanceName; 
                serverMap.set(instanceData.instanceUrl, instanceData);
				changePumaServerDropdown.appendChild(opt);
			}
		};
		xhr.open('GET', serverUrl, true);
		xhr.send();
	}
}
    
changePumaServerDropdown.addEventListener("change", function(){
    chrome.runtime.sendMessage({ type: "Update icons", instanceData: serverMap.get(this.value)});
});

//Restores the selected PUMA Instance from the last time the extension was used
chrome.storage.local.get('pumaInstance', function (e) {
	var instanceData = e.pumaInstance;
	var home = instanceData.instanceUrl;
	changePumaServerDropdown.value = home;
});

updateButtons();

chrome.runtime.onMessage.addListener((request) => {
	if (request.type == "Update Button Sources") {
		updateButtons();
	}
});

function updateButtons(){
	console.log("Updating button sources");
	chrome.storage.local.get('pumaInstance', function (e) {
		var instanceData = e.pumaInstance;
		document.getElementById("homeButton").src = instanceData.homeButtonImgUrlChrome;			
		document.getElementById("bookmarkButton").src = instanceData.bookmarkButtonImgUrlChrome;
		document.getElementById("publicationButton").src = instanceData.publicationButtonImgUrlChrome;		
	});
}