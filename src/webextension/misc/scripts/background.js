//check if the lastUpdate in localstorage is older than 7 days and update the icon if so
function checkLastUpdate(){
	chrome.storage.local.get('lastUpdate', function (e) {
		var lastUpdate = e.lastUpdate;
		if (typeof lastUpdate !== "undefined") {
			var now = new Date();
			var lastUpdateDate = new Date(lastUpdate);
			var diff = now.getTime() - lastUpdateDate.getTime();
			var days = Math.floor(diff / (1000 * 60 * 60 * 24));
			if (days > 7) {
				chrome.storage.local.get('pumaInstance', function (e) {
					var instanceData = e.pumaInstance;
					if (typeof instanceData !== "undefined") {
						updateIcon(instanceData);
						chrome.storage.local.set({ 'lastUpdate': now.toISOString() });
					}
				});
			}
		}
	});
}

var SERVER_SETTING_TAB = null;

function showServerList(){
	chrome.tabs.create({'url': chrome.extension.getURL('../ui/serverList.html')}, function(tab){
		SERVER_SETTING_TAB = tab;
	});
}

function closeServerList(){
	chrome.tabs.remove(SERVER_SETTING_TAB.id, function(){ });
}

function updateIcon(instanceData){
	var canvas = document.createElement("canvas");
    var context = canvas.getContext("2d");
    var img = document.createElement("img");
    img.crossOrigin = "anonymous";
    img.onload = function () {
        context.drawImage(img, 0, 0);
        chrome.browserAction.setIcon({ imageData: context.getImageData(0, 0, this.width, this.height) });
    };
    img.onerror = function () { console.log("Error loading image"); };
    img.src = instanceData.popUpIcon.replace('http://','https://');
}

chrome.runtime.onMessage.addListener((request) => {
    if(request.type == "Close server list"){
        chrome.storage.local.set({ 'hasRun': true });
		chrome.storage.local.set({ 'lastUpdate': new Date() });
		chrome.storage.local.set({ 'pumaInstance': request.instanceData });
        closeServerList();
		updateIcon(request.instanceData);
		chrome.runtime.sendMessage({ type: "Update Button Sources" });
    }

	if(request.type == "Show server list"){
        showServerList();
    }

    if(request.type == "Update icons"){
        chrome.storage.local.set({ 'pumaInstance': request.instanceData });
		chrome.storage.local.set({ 'lastUpdate': new Date().toISOString });
        updateIcon(request.instanceData);
		chrome.runtime.sendMessage({ type: "Update Button Sources" });
    }

	if(request.type == "Check last update"){
		checkLastUpdate();
	}
});